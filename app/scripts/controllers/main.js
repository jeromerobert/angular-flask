'use strict';

/**
 * @ngdoc function
 * @name angularFlaskApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularFlaskApp
 */
angular.module('angularFlaskApp')
  .controller('MainCtrl', ['$scope', 'autotests', function ($scope, autotests) {
    autotests.getList().then(function(messages) {
      $scope.tests = messages.data;
    });
    $scope.updateEps = function() {
      var that = this;
      autotests.updateEps(this.key, this.resultUrl, this.url, this.epss.eps).then(function(){
        that.epss.refeps = that.epss.eps;
      });
    };
    $scope.deleteRef = function() {
      var that = this;
      autotests.deleteRef(this.key, this.resultUrl, this.url).then(function(){
        delete that.$parent.references[that.url];
      });
    };
  }]);
