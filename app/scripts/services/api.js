'use strict';

/**
 * @ngdoc service
 * @name angularFlaskApp.autotests
 * @description
 * # autotests
 * Service in the angularFlaskApp.
 */
angular.module('angularFlaskApp')
  .service('autotests', ['$http', function ($http) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    this.getList = function () {
      return $http.get('/list');
    };
    this.updateEps = function(testId, resultUrl, refUrl, newEps) {
      return $http.put('/update/'+testId+'/'+encodeURIComponent(resultUrl),
         {'refuri': refUrl, 'refeps': newEps}
      );
    };
    this.deleteRef = function(testId, resultUrl, refUrl) {
      return $http.delete('/update/'+testId+'/'+encodeURIComponent(resultUrl),
         {params: {'refuri': refUrl}}
      );
    };

  }]);
