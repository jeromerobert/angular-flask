#! /usr/bin/env python
from flask import Flask, send_from_directory
from flask_restful import reqparse, abort, Api, Resource
import yaml

#app = Flask(__name__, static_folder='dist', static_url_path='')
app = Flask(__name__)
api = Api(app)

def read_db():
    with open('tests.yml') as f:
        return yaml.load(f)

def write_db(db):
    with open('tests.yml', 'w') as f:
        return f.write(yaml.dump(db))

class List(Resource):
    def get(self):
        return read_db()

class EditEps(Resource):
    def put(self, test_id, result_uri):
        parser = reqparse.RequestParser()
        parser.add_argument('refuri')
        parser.add_argument('refeps')
        args = parser.parse_args()
        db = read_db()
        db[test_id][result_uri][args['refuri']]['refeps'] = float(args['refeps'])
        #abort(404, message="invalid test_id")
        write_db(db)
        return '', 201

class UpdateReference(Resource):
    def put(self, test_id, result_uri):
        parser = reqparse.RequestParser()
        parser.add_argument('refuri')
        parser.add_argument('refeps')
        args = parser.parse_args()
        db = read_db()
        eps = float(args['refeps'])
        db[test_id][result_uri][args['refuri']] = {'eps': eps, 'refeps': eps}
        write_db(db)
        return '', 201

    def delete(self, test_id, result_uri):
        parser = reqparse.RequestParser()
        parser.add_argument('refuri')
        args = parser.parse_args()
        db = read_db()
        print db[test_id][result_uri], args
        try:
            del db[test_id][result_uri][args['refuri']]
        except KeyError:
            return '', 404
        write_db(db)
        return '', 204

api.add_resource(List, '/list')
api.add_resource(UpdateReference, '/update/<path:test_id>/<path:result_uri>')
api.add_resource(EditEps, '/edit/<string:test_id>/<string:result_uri>')

@app.route('/<path:path>')
def serve_page(path):
    if path.startswith('bower_components'):
        return send_from_directory('.', path)
    else:
        return send_from_directory('app', path)

@app.route('/')
def root():
    return send_from_directory('app', 'index.html')

if __name__ == '__main__':
    print yaml.dump(read_db())
    app.run(debug=True)
