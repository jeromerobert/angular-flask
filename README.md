## Testing

```
pip install --user flask-restful
npm install
./api.py
```

Then click buttons and see how `tests.yml` change.

## Development

Swap `_devDependencies` and `devDependencies` in `packages.json` to get `grunt` and `yo`.
