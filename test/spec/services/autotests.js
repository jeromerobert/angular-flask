'use strict';

describe('Service: autotests', function () {

  // load the service's module
  beforeEach(module('angularFlaskApp'));

  // instantiate service
  var autotests;
  beforeEach(inject(function (_autotests_) {
    autotests = _autotests_;
  }));

  it('should do something', function () {
    expect(!!autotests).toBe(true);
  });

});
